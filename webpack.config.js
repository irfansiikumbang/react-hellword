var path = require('path')
var webpack = require('webpack')

var AppCachePlugin = require('appcache-webpack-plugin')

module.exports = {
  devtool: 'cheap-eval-source-map',
  entry: [
      "webpack/hot/only-dev-server", // See above
      path.resolve(__dirname, 'app.js')
  ],
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: "bundle.js"
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
	  new AppCachePlugin()
  ],
  module: {
    loaders: [{
      test: /\.js$/,
      loaders: [ 'babel' ],
      exclude: /node_modules/,
      include: __dirname
    }]
  }
}
