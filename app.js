import React from 'react'
import { render } from 'react-dom'

import HelloWord from './components/HelloWord';

render(<HelloWord />, document.getElementById('app'));
